import Vue from 'vue';
import Button from '../src/components/Buttons/Button.vue';

export default { title: 'Button' };

Vue.component('ui-button', Button);

export const Default = () => '<ui-button>Default button</ui-button>';

export const Styled = () => ({
  components: { Button },
  data() {
    return {
      loading: false
    };
  },
  template: `
    <div>
      <div class="mb-2">
        <label for="loading">
          <input id="loading" v-model="loading" type="checkbox" />
          Check loading
        </label>
      </div>
      <div class="d-flex">
        <Button primary :loading="loading" extend-class="mr-2">Primary</Button>
        <Button secondary :loading="loading" extend-class="mr-2">Secondary</Button>
        <Button success badge="5" :loading="loading" extend-class="mr-2">Success</Button>
        <Button warning :loading="loading" extend-class="mr-2">Warning</Button>
        <Button danger :loading="loading" extend-class="mr-2">Danger</Button>
        <Button info :loading="loading" extend-class="mr-2">Info</Button>
      </div>
    </div>
  `
});
